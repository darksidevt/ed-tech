#!bash
git submodule update --init --recursive
pip install -U -r requirements.txt
mkdocs serve --dev-addr=0.0.0.0:8000