---
tags:
    - python
    - docker
    - containerization
    - tutorial
---
# Using containers to develop Python applications

## Why use containers?
Some people use pyenv to manage their Python versions. Others use virtualenv. I use a container. Take a look at the (Container vs. Docker)[Container vs Docker.md] writeup to see why it' proper to generalize container development vs Docker development. I like using Docker because it's a well know name in the technology industry but, any reference to Docker is not specific and could be used with any OCI compliant application.

So, why containerize? With Docker I can also easily share my development environment with others. This is especially useful when I'm working on a project with other people. I can share my Dockerfile and docker-compose.yml files with them and they can easily spin up a development environment that matches mine. Since most companies are starting to or have been using containerization for a while now, people don't need any specific python knowledge to get started with a python project. Using a container as a common knowledge item, anyone with Docker knowledge can get started looking over, running, or collaborating with python code. If someone is well versed in Python, containers allow for a cleaner install of Python on their local PC since nothing is actually installed on it. This is especially useful for people who are working on multiple projects that require different versions of Python. Containers allow for a clean separation of Python versions and dependencies.

## How do I get started?
I've got an example that I use for my development that also installs Docker if I need access to the docker daemon during development.
```py title="Dockerfile" linenums="1"
--8<-- "docs/Docker/python-docker/dockerfile"
```
```bash title="init.bash" linenums="1"
--8<-- "docs/Docker/python-docker/init.bash"
```

The first file is the instructions for how you want the container to be made. In the instructions, I set my working directory in line 2, copy the init.bash file that sets the timezone and installs docker if the $DOCKER ARG is set, and set the timezone in line 4. Finally, the init.bash file is run to install everything setup in the previous lines. To try to keep it clean, there is some standard cleanup that happens at the end of the init.bash file execution in lines 14-18.

Create the 2 files then build the container with the following command in your terminal while in the directory that you made the files:
```bash
docker build -t python-dev .
```

Congrats! You've just made your first container!

## How do I use it?
That's great and all but, how do we use it. The fasted way to get started is to open a terminal window in the directory that your python project is in and run the following command:
```bash
docker run -it --rm -v ${pwd}:/data python-dev /bin/bash
```
This command will run the container that you just built and mount the current directory to the /data directory in the container. This will allow you to edit your files in your favorite editor and have them be reflected in the container. The -it flag will allow you to interact with the container and the --rm flag will remove the container when you exit it. The /bin/bash at the end of the command will open a bash shell in the container. When you are ready to test out your python application, from the terminal with windows that is now in the container, you can run any command as you normally would installing PyPi packages and running your application. For example, if you have a python application named main.py you can run the following command:
```bash
python main.py
```
This will run your application just as it would if you had python installed on your local machine.

## Extra Credit
Some editors like [VSCode](VSCode.md) have extensions to connect to a container and allow you to edit files in the container. This allows you to keep using all the great extensions from VSCode with your newly created container. 

## Wrapping it up
Containers are a great way to develop Python applications. They allow you to easily share your development environment with others and keep your local machine clean. If you are interested in learning more about Docker, check out the [Docker documentation](https://docs.docker.com/get-started/) and to learn more about containers in general take a look at my writeup on [Container vs Docker](Container_vs_Docker.md).